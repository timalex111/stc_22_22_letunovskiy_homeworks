import java.util.Scanner;

public class HomeWork {

    //добавили функцию посчитать элементы массива
    // добавили диапозоны от from до to
    public static int calcSumOfArrayRange(int[] a, int from, int to) {
        int sum = 0;
        // i++ это i + 1
        // a += b это a = a + b
        for (int i = from; i <= to; i++) {
            sum += a[i];
        }
        return sum;
    }
    //функция понять четные или нет
    public static boolean isEven(int number){
        return number % 2 == 0;
    }
     public static void main(String[] args) {
         int[] array = {34, 54, 16, 45, 27, 33, 52, 68, -15, -4, -55};
         int[] array1 = {34, 54, 16, 45, 27, 33, 52, 68, -15, -4, -55};
         //теперь используем эту функцию в этом методе(ctrl+b что бы посмотреть где эта функция)
         int sum1 = calcSumOfArrayRange(array, 0, 10);

         System.out.println(sum1);
         int sum2 = calcSumOfArrayRange(array1, 2,6);
            // добавили ввод через клавиатуру
         Scanner scanner = new Scanner(System.in);
         //добавили переменную для числа с клавиатуры
         int n = scanner.nextInt();
        // добавили переменную правда/нет, и обратились к функции выше
         boolean isEven = isEven(n);
            // в переменную добавили функцию
         if (isEven(n)) {
             System.out.println("Число четное!");
         }else {
             System.out.println("Число нечетное! ");

         }
     }


    }



