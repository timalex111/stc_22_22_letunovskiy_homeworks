import java.util.Arrays;

public class Main2 {
    public static void main(String[] args) {
        int min, indexofMin;
        int[] array = {10, 13, 11, 12, 2, 4};

        for(int i = 0; i < array.length; i++){
        min = array[i];
        indexofMin = i;
        for( int j = i; j < array.length; j++){
            if (array[j] < min) {
                min = array[j];
                indexofMin = j;
            }
        }
        int temp = array[i];
        array[i] = array[indexofMin];
        array[indexofMin] = temp;

    }
        System.out.println(Arrays.toString(array));
    }

}
