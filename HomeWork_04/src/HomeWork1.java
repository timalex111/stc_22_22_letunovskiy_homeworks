import com.sun.source.tree.BreakTree;

public class HomeWork1 {
    private static int sumInterval(int left, int right ){
        if (left > right){
            return -1;
        }
        int accumulator = 0;
        for (int i = left; i <= right; i++){
            accumulator += i;

        }
        return accumulator;
    }

}
