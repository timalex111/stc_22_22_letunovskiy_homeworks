import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;

public class Bankomat {
    private static final AtomicInteger COUNTER = new AtomicInteger(1);
   private int id;

    public Bankomat() {
        id = COUNTER.getAndIncrement();
    }

    public int getId() {
        return id;
    }


    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String cardNum = "123456789"; // ����� �����
        int pwd = 1234;        // ������
        boolean flag = true;   // ��������� ���������� ����������
        long surplus = 10000; // ������

        // ��������� //
        System.out.println(" ����� ���������� � �������� !");

        // ������������ ���������� ������� //
        for (int i = 1; i <= 3; i++) {
            System.out.println("����������, ������� ����� �����:");
            String inputCard = input.next();
            System.out.println("����������, ������� ��� ������:");
            int inputPwd = input.nextInt();

            // ��������� ������� � ������ //
            if (inputCard.equals(cardNum) && inputPwd == pwd) {
                flag = true;
                break;
            } else {
                if (i <= 2) {
                    System.out.println("��������, �������� ������, � ��� ���� " + (3 - i) + "" + " �������!");
                } else {
                    System.out.println("��������, ���� ����� �������������!");
                    break;
                }
                flag = false;
            }
        }

        // �������� ������� ����� ��������� ����� � ������� //
        if (flag) {
            char answer = 'y';
            while (answer == 'y') {
                System.out.println("����������, �������� �������: 1. ����� ������� 2. ���������� ����� 3. �������� ������� 4. ������� 5. �����");
                int choice = input.nextInt();
                switch (choice) {
                    case 1:
                        // ��������� �������� ������ //
                        System.out.println("-> ����� �������");
                        System.out.println("����������, ������� ����� ������:");
                        long getMoney = input.nextLong();
                        if (getMoney > 0) {
                            if (getMoney <= surplus) {
                                if (getMoney % 100 == 0) {
                                    System.out.println("����������, �������� ���� ��������! ������ " + (surplus - getMoney) + " ������");
                                    System.out.println("���������� ����������� �������� " + new Bankomat().getId());
                                } else {
                                    System.out.println("��������, � �� ���� ������� ���������!");
                                }
                            } else {
                                System.out.println("��������, ������ ������������!");
                            }
                        } else {
                            System.out.println("����������, ������� ���������� �����:");
                        }
                        break;
                    case 2:
                        // ��������� ���������� �������� //
                        System.out.println("-> �������");
                        System.out.println("����������, ����������� �������� � ��������� �� � ���������� ����:");
                        long saveMoney = input.nextLong();
                        if (saveMoney > 0 && saveMoney <= 10000) {
                            if (saveMoney % 100 == 0) {
                                surplus += saveMoney;
                                System.out.println("������� �������! ������ " + surplus + " ������");
                                System.out.println("���������� ����������� �������� " + new Bankomat().getId());
                            } else {

                                long backMoney = saveMoney % 100;
                                surplus = (saveMoney + surplus - backMoney);
                                System.out.println("������� �������! ������ " + surplus + " ������");
                                System.out.println("���������� ����������� �������� " + new Bankomat().getId());
                                System.out.println("����������, �������� ����� " + backMoney);
                            }
                        } else if (saveMoney > 10000) {
                            System.out.println("������� ������� �� 10000 ������ �� ���, ����������, ������� ������� ��������!");
                        } else {
                            System.out.println("�������������� �������� �������� ���������� ���������, � ��� ��������������� � ������������!");
                            System.out.println("���������� ����������� �������� " + new Bankomat().getId());
                        }
                        break;
                    case 3:
                        // ��������� ������ ������� //
                        System.out.println("-> ��������� ������");
                        System.out.println("������ �� ����� �����:" + surplus + " ������");
                        System.out.println("���������� ����������� �������� " + new Bankomat().getId());
                        break;
                    case 4:
                        // ��������� �������� �������� //
                        System.out.println("-> ��������");
                        System.out.println("����������, ������� ����� ��������:");
                        long goMoney = input.nextLong(); // ����� ��������
                        if (goMoney > 0) {
                            if (goMoney <= surplus) {
                                System.out.println("������� �������! ������" + (surplus - goMoney) + " ������");
                                System.out.println("���������� ����������� �������� " + new Bankomat().getId());
                            } else {
                                System.out.println("��������, ����������, ���������, ��� � ��� ���������� ������� �� �����!");
                                System.out.println("���������� ����������� �������� " + new Bankomat().getId());
                            }

                        } else {
                            System.out.println("������� �� ��������! ����������, ������� ���������� �����:");
                            System.out.println("���������� ����������� �������� " + new Bankomat().getId());
                        }
                        break;
                    case 5:
                        // ��������� �������� ������ //
                        System.out.println("���������� ����������� �������� " + new Bankomat().getId());
                        System.out.println("����������� ����� �� ���������...");
                        System.out.println("������� �� ���� �������������!");
                        return;
                    default: {
                        System.out.println("��������, ��������� ���� ������� �������!");
                        System.out.println("���������� ����������� �������� " + new Bankomat().getId());
                        break;
                }

                }
            }
        }
    }
}



