package Main;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ProductRepositoryFileBasedImpl implements ProductRepository {

    private final List<Product> products;

    public ProductRepositoryFileBasedImpl(File db) {
        this.products = readDataBase(db);
    }

    @Override
    public Product findByID(int id) {
        for (var product : products) {
            if (product.getId() == id) {
                return product;
            }
        }
        return null;
    }

    private static List<Product> readDataBase(File db) {
        try (BufferedReader reader =
                     new BufferedReader(new InputStreamReader(new FileInputStream(db)))) {
            return reader.lines()
                    .map(ProductRepositoryFileBasedImpl::parseString)
                    .collect(Collectors.toList());

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static Product parseString(String line) {
        int startIndex = 0;
        StringBuilder sb = new StringBuilder();

        // parse id
        startIndex = fillBetween(line, startIndex, sb);
        int id = Integer.parseInt(sb.toString());
        sb.setLength(0);

        // parse name
        startIndex = fillBetween(line, startIndex, sb);
        String name = sb.toString();
        sb.setLength(0);

        // parse price
        startIndex = fillBetween(line, startIndex, sb);
        double price = Double.parseDouble(sb.toString());
        sb.setLength(0);

        // parse count
        startIndex = fillBetween(line, startIndex, sb);
        int count = Integer.parseInt(sb.toString());
        sb.setLength(0);

        return new Product(id, name, price, count);
    }

    private static int fillBetween(String line, int from, StringBuilder to) {
        for (int pos = from; pos < line.length(); pos++) {
            char c = line.charAt(pos);
            if (c == '|') {
                return pos + 1;
            }
            to.append(c);
        }
        return line.length();
    }


    }
