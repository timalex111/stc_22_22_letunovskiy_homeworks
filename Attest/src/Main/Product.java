package Main;

import java.util.Objects;

public class Product {

        private final int id;
        private final String name;
        private final double price;
        private final int count;

        public Product(int id, String name, double price, int count) {
                this.id = id;
                this.name = name;
                this.price = price;
                this.count = count;
        }

        public int getId() {
                return id;
        }

        public String getName() {
                return name;
        }

        public double getPrice() {
                return price;
        }

        public int getCount() {
                return count;
        }

        @Override
        public boolean equals(Object o) {
                if (this == o) {
                        return true;
                }
                if (o == null || getClass() != o.getClass()) {
                        return false;
                }
                Product product = (Product) o;
                return id == product.id && Double.compare(product.price, price) == 0
                        && count == product.count && Objects.equals(name, product.name);
        }

        @Override
        public int hashCode() {
                return Objects.hash(id, name, price, count);
        }

        @Override
        public String toString() {
                return "Product{" +
                        "id=" + id +
                        ", name='" + name + '\'' +
                        ", price=" + price +
                        ", count=" + count +
                        '}';
        }
}


