import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Human timur = new Human(1.76, true);
        //timur.height = 1.76;
        //timur.isWorker = true;

        Human nottimur = new Human(1.85, false);
        //nottimur.height = 1.85;
        //nottimur.isWorker = false;
        Scanner scanner = new Scanner(System.in);
        timur.height = scanner.nextDouble();

        timur.grow(0.05);
        timur.relax();

        nottimur.grow(0.30);
        nottimur.work();

        System.out.println(timur.height + " " + timur.isWorker);
        System.out.println(nottimur.height + " " + nottimur.isWorker);
    }

}