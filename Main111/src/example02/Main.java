package example02;
//Создать массив из 1000 людей
//Каждый имеет возраст от 18 - 90
// Нужно понять люди какого возраста чаще других встречаются
// Решить нужно за О(n)

import java.util.Arrays;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        int min = User.MIN_ADULT_AGE;
        int max = User.MAX_ADULT_AGE + 1;
        //Массив людей, пока пустой
        User[] users = new User[1000];
        //генератор случайных чисел
        Random random = new Random();
        //пробегаюсб по всем элементам массива и создаю человека для каждого элемента
        for (int i = 0; i < users.length; i++) {
            //имя каждого User +1
            String name = "User" + i;
            int age = random.nextInt(max - min) + min;
            //сщздаю на i - ой позиции в массиве нового пользователя
            users[i] = new User(name, age);
        }
       // users[14].age = 150;
        int[] counts = new int[73];
        for (int i = 0; i < users.length; i++) {
//            //Получили текущего пользователя
//            User currentUser = users[i];
//            //получить его возраст
//            int currentAge = currentUser.getAge();
//            counts[currentAge]++;
            counts[users[i].getAge() - min]++;
        }
        for(int i = 0; i < counts.length; i++){
            System.out.println((i + 18) + " - " + counts[i]);
        }
    }
}
